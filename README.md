# curso typescript


<h2>Anotaciones</h2>
<p>Es un superconjunto tipado de javascript, que compila a javascript.</p>
<p><strong>Lenguaje de programación tipado:</strong> Posee un conjunto de tipos para poder usarlos con las variables, pudiendo personalizarlos o extenderlos.</p>
<p><strong>Lenguaje de alto nivel:</strong> Entendible por humanos y posee un alto nivel de abstracción del código máquina.</p>
<p><strong>Genera como resultado código JavaScript</strong>: Emite código javascript compatible con browsers y otras herramientas de javascript.</p>
<p><strong>Código abierto.</strong></p>
<p><strong>Desarrollo desde cualquier sistema.</strong></p>
<p><strong>El código puede ejecutarse en cualquier navegador o plataforma que soporte javascript.</strong></p>
&lt;h3&gt;Porque usar TypeScript&lt;/h3&gt;
<ul>
  <li>Programación orientada a objetos</li>
  <li>Potenciar tu código JavaScript</li>
  <li>Mayor productividad</li>
  <li>Poderoso sistema de tipos</li>
  <li>Compila a ES5, ES6 y más</li>
  <li>Proyecto muy activo/Open source</li>
  <li>Actualizaciones periódicas</li>
  <li>Comunidad creciente</li>
  <li>Puede prevenir cerca del 15% de bugs</li>
  <li>Puede usar TypeScript para backend**__**</li>
</ul>

# El archivo de configuración de TypeScript

<p>Podemos configurar nuestro compilador con el archivo “tsconfig.json”.</p>
<ul>
<li>Especifica la raíz de un proyecto TypeScript</li>
<li>Permite configurar opciones para el compilador</li>
<li>Para generar el archivo escribimos <code>tsc --init</code></li>
</ul>
<p>El archivo generado vendrá muy bien documentado para saber qué es lo que hace cada configuración.</p>
<p>Podemos usar de 3 formas diferentes el TSC:</p>
<ul>
<li><code>tsc</code> -&gt; Busca la configuración</li>
<li><code>tsc --project platzi</code> -&gt; Especifica un directorio que contiene la configuración</li>
<li><code>tsc file.ts</code> -&gt; Omite la configuración</li>
</ul>

---------------------------------------------
<p>El archivo de configuración de TS tsconfig.json  permite:</p>
<ul>
<li>Especificar la raíz de nuestros archivos TS para el proyecto</li>
<li>Permite configurar las opciones que se envían como parámetros&nbsp; al compilador de TS</li>
</ul>
<p>Para generar el archivo de configuración utilizamos el comando <strong>++tsc --init ++</strong> este archivo consta de lo siguiente:</p>
<pre><code>tsconfig.json
{
    <span class="hljs-string">"extendes"</span>: <span class="hljs-string">"-/configs/base"</span>, <span class="hljs-comment">// para importar configuraciones de otro archivo</span>
    <span class="hljs-string">"compileOnsave"</span>: ture, <span class="hljs-comment">// para indicar al ide o editor que cada vez que se produscan cambios se compile automaticamente</span>
    <span class="hljs-string">"compilerOptions"</span>:{
        <span class="hljs-string">"target"</span>: <span class="hljs-string">"es5"</span>,
        <span class="hljs-string">"module"</span>: <span class="hljs-string">"commonjs"</span>,
        <span class="hljs-string">"strict"</span>: true,
        <span class="hljs-string">"removeComments: true</span>
    },
    <span class="hljs-string">"include"</span>:[ <span class="hljs-comment">// para incluir directorios o subdirectorios</span>
    <span class="hljs-string">"src/**/*.ts"</span>
    ],
    <span class="hljs-string">"exclude"</span>: [ <span class="hljs-comment">// para excluir directorios o subdirectorios</span>
        <span class="hljs-string">"node_modules"</span>,
        <span class="hljs-string">"**/*.test.ts"</span> 
     ]
}
</code></pre>
<p><strong>target</strong> : Nos indica la versión de ECMAScript que se generaŕa al final de la compilación del tsc</p>
<ul>
<li><strong>module</strong>: Es en donde especificamos la configuración para la generación de módulos en el proyecto usando valores como (Num, Common.js,AMD, System.js etc…)</li>
<li><strong>strict</strong>: Es utilizado para habilitar o deshabilitar el chequeo estricto de tipos de nuestro proyecto.</li>
<li><strong>remove</strong> Comments: Para remover todos los comentarios que se encuentren en nuestro proyecto</li>
<li><strong>includes</strong>:  Podemos especificar opciones adicionales que van de la mano con la configuración en este podemos especificar los directorios y subdirectorios forman parte de los archivos procesados por el compilador</li>
<li><strong>exclude</strong>:  A diferencia del include este es para excluir directorios o subdirectorios del procesamiento del compilador</li>
</ul>

# Tipado en TypeScript

<ul>
<li>Explícito: Define una sintaxis para la creación de variables con tipo de datos.</li>
<li>Inferido: TypeScript tiene la habilidad de “deducir” el tipo de función de un valor.</li>
<li>Tipado explícito -&gt; <code>mi_variable : Int</code></li>
<li>Tipado implícito -&gt; <code>mi_variable = 5</code></li>
</ul>


------------------------------------------
<p><strong>MY NOTES 🤓 TYPED IN JAVASCRIPT</strong></p>
<ul>
<li>Explicito</li>
</ul>
<p>Define una sintaxis para la creación de variables con tipo de dato.</p>
<p>Significa que tenemos que tener en cuenta el definir un tipo de dato a la hora de declarar una variable</p>
<ul>
<li>Inferido</li>
</ul>
<p>Typescript tiene la habilidad de “deducir” el tipo en función de un valor</p>
<p><strong>Tipo de dato Explicito</strong></p>
<pre><code class="language-jsx">nombreVariable : TipodeDato
</code></pre>
<p>:  → permite especificar el tipo de dato</p>
<p><strong>Tipo de dato Inferido</strong></p>
<pre><code class="language-jsx">nombreVar = valor
</code></pre>
<p>nomVar → typescript deduce el tipo de dato</p>
<p>valor → la variable debe estar inicializada</p>
<p><strong>Tipos de datos basicos</strong></p>
<p>Las mas utilizadas en js</p>
<ul>
<li>cadenas de texto</li>
<li>numeros</li>
<li>objetos</li>
<li>arreglos</li>
<li>booleanos</li>
</ul>
<p><strong>Tipos primitivos en typescript</strong></p>
<ul>
<li>Number</li>
<li>Boolean</li>
<li>String</li>
<li>Array</li>
<li>Tuple</li>
<li>Enum</li>
<li>Any</li>
<li>Void</li>
<li>Null</li>
<li>Undefined</li>
<li>Never</li>
<li>Object</li>
</ul>

# Void y Never

<p><strong>void</strong> se utiliza para identificar funciones que terminan y no retornan algún valor; y <strong>never</strong> es una función que nunca termina y no da algún retorno.</p>
<p><strong>void</strong> podría ser usado en una función que temina solo haciendo el log de la info de un usuario.</p>
<p><strong>never</strong> podría ser usado en una función que cada x tiempo esta mandando estadisticas de la app. Por lo que nunca termina.</p>

----------------------------------
<p>El detalle está en que las funciones tipo <strong>void</strong> realizan algo y terminan su flujo (sentencias, instrucciones…), por defecto al no retorna nada (ningún dato) devuelven undefined (pero terminan y devuelven algo).  En cambio las tipo <strong>never</strong> son las que nunca retornar, se podría ver como que no terminan sus instrucciones, por ello los ejemplos son lanzando una excepción (que por definición interrumpe la ejecución normal del código) y generando un bucle infinito.<br>
Esa es la gran diferencia entre <strong>void</strong> y <strong>never</strong>, la primera termina su código pero no retorna datos, la otra no termina y, por ende, nunca retorna.<br>
<strong>never</strong>  en TypeScript es muy útil precisamente para manejo de errores como se comentó en el ejemplo</p>

# object

<p>Se me ocurre que puede ser de utilidad cuando uno quiere evitar mutar una variable. En el ejemplo voy a guardar user en una variable auxiliar newUser,  luego voy a  actualizar el nombre del primer usuario y finalmente  voy a mostrar sus resultados.</p>
<p>caso 1) Object</p>
<pre><code><span class="hljs-keyword">const</span> user = { 
  <span class="hljs-attribute">id:</span><span class="hljs-emphasis"> 1, 
  name</span>: <span class="hljs-string">'Harry'</span> 
}

<span class="hljs-keyword">const</span> newUser = user
user.name = <span class="hljs-string">'Henry'</span>; 
<span class="hljs-built_in">console</span>.log(<span class="hljs-string">'user'</span>, user); <span class="hljs-comment">// user { id: 1, name: 'Henry' }</span>
<span class="hljs-built_in">console</span>.log(<span class="hljs-string">'newUser'</span>, newUser); <span class="hljs-comment">// newUser { id: 1, name: 'Henry' }</span>
</code></pre>
<p>caso 2):  object</p>
<pre><code><span class="hljs-keyword">let</span> <span class="hljs-attribute">user</span>: object;
user = {}
user = { 
  <span class="hljs-attribute">id:</span><span class="hljs-emphasis"> 1, 
  name</span>: <span class="hljs-string">'Harry'</span> 
}

<span class="hljs-keyword">const</span> newUser = user
<span class="hljs-comment">// user.name = 'Henry' // Error</span>
user = {...user, <span class="hljs-attribute">name</span>: <span class="hljs-string">'Henry'</span>} 
<span class="hljs-built_in">console</span>.log(<span class="hljs-string">'user'</span>, user); <span class="hljs-comment">// user { id: 1, name: 'Henry' }</span>
<span class="hljs-built_in">console</span>.log(<span class="hljs-string">'newUser'</span>, newUser); <span class="hljs-comment">// newUser { id: 1, name: 'Harry' }</span>
</code></pre>
<p>Como podemos observar debido a que en el caso 1 es posible acceder a la propiedad name se puede llegar a mutar la variable lo que puede causar errores, mientras que para el caso 2 debido a que no es posible acceder a las propiedades del objeto nos vemos forzados a buscar otra alternativa que causa que las variables no muten.</p>


# Array

<p>Me parece bien utilizar la nomenclaruta de <code>variable: tipo[ ]</code> para arreglos rapidos y de pocos elementos que vaya a ocupar en mi proyecto.<br>
Y usar <code>variable: Array&lt;tipo&gt;</code> para aquellos arreglos que van a contener muchos elementos y su longitud estara variando a lo largo de la aplicacion.</p>


----------------------------------
<p>En la mayoría de lenguajes de alto nivel no se aplica la definición estricta de “array”, ya que según su <a href="https://computersciencewiki.org/index.php/Arrays" rel="nofollow noopener" target="_blank">definición</a> son un conjunto de datos <strong>del mismo tipo</strong>.</p>
<p>En TypeScript podemos usar la definición estricta de “array”, la que se tiene en lenguajes como JavaScript en los que podemos tener varios tipos de dato en el mismo array o incluso un término medio en el que indicamos una serie de tipos que puede contener el array.</p>
<pre><code><span class="hljs-comment">// definición estricta de arrays</span>
<span class="hljs-keyword">let</span> users: <span class="hljs-built_in">string</span>[] = [<span class="hljs-string">"John"</span>, <span class="hljs-string">"Jane"</span>];

<span class="hljs-comment">// arrays según JavaScript (y otros lenguajes)</span>
<span class="hljs-keyword">let</span> users: <span class="hljs-built_in">any</span>[] = [<span class="hljs-string">"a"</span>, <span class="hljs-number">1</span>, <span class="hljs-literal">true</span>];

<span class="hljs-comment">// especificar tipos de dato</span>
<span class="hljs-keyword">let</span> things: <span class="hljs-built_in">Array</span>&lt;<span class="hljs-built_in">number</span> | <span class="hljs-built_in">boolean</span>&gt; = [<span class="hljs-number">32</span>, <span class="hljs-literal">true</span>];
<span class="hljs-comment">// or</span>
<span class="hljs-keyword">let</span> things: (<span class="hljs-built_in">number</span> | <span class="hljs-built_in">boolean</span>)[] = [<span class="hljs-number">32</span>, <span class="hljs-literal">true</span>];
</code></pre>

# Tupla

Tupla: Permite expresar un arreglo con un número fijo de elementos. Los tipos de datos son conocidos.

    let userInfo = [number,string];
    userInfo = [1,‘danijazzero’];

# Enum

<p>La manera de <strong>extender</strong> un enum en TypeScript es simplemente asignando nuevos valores al enum que queremos extender, es decir:</p>
<pre><code><span class="hljs-class"><span class="hljs-keyword">enum</span> <span class="hljs-title">Color</span> {</span>
    Red,
    Green,
    Blue
}

<span class="hljs-class"><span class="hljs-keyword">enum</span> <span class="hljs-title">Color</span> {</span>
    DarkRed = <span class="hljs-number">3</span>,
    DarkGreen,
    DarkBlue
}
</code></pre>
<p>Tomando en cuenta que es necesario <em>reinicializar</em> el primer elemento del segundo enum en continuación del primero.<br>
.<br>
Esto se puede lograr ya que una vez compilado nuestro código a JavaScript un enum no deja de ser un objeto literal, es por eso que se pueden asignar nuevos valores.<br>
.<br>
Fuente:<br>
<a href="https://basarat.gitbook.io/typescript/type-system/enums#enums-are-open-ended" rel="nofollow noopener" target="_blank">Enums are open ended - TypeScript Deep Dive</a><br>
<a href="https://basarat.gitbook.io/typescript/type-system/enums#number-enums-and-strings" rel="nofollow noopener" target="_blank">Number Enums and Strings - TypeScript Deep Dive</a></p>

# Unión de Tipos, Alias y Tipos Literales
&lt;h5&gt;Unión de Tipos&lt;/h5&gt;
<ul>
<li>En TypeScript se puede definir una variable con múltiples tipos de datos: <code>Union Type</code></li>
<li>Se usa el símbolo de pipe <code>|</code> entre los tipos</li>
</ul>
&lt;h5&gt;Alias de Tipos&lt;/h5&gt;
<ul>
<li>TypeScript permite crear un alias como nuevo nombre para un tipo</li>
<li>El alias se puede aplicar también a un conjunto de combinación de tipos</li>
<li>Se usa la palabra reservada <code>type</code></li>
</ul>
&lt;h5&gt;Tipos Literales&lt;/h5&gt;
<ul>
<li>Una variable con un tipo literal puede contener únicamente una cadena del conjunto</li>
<li>Se usan cadenas como “tipos”, combinados con el símbolo de pipe | entre ellos</li>
</ul>
<pre><code class="language-typescript"><span class="hljs-keyword">export</span> {}

<span class="hljs-comment">//* Por ejemplo, tenemos usuarios que tienen un ID numérico o de tipo string 10, '10'</span>
<span class="hljs-keyword">let</span> idUser: <span class="hljs-built_in">number</span> | <span class="hljs-built_in">string</span> <span class="hljs-comment">//* Aceptará strings y number</span>
idUser = <span class="hljs-number">10</span>
idUser = <span class="hljs-string">'10'</span>

<span class="hljs-comment">//* Buscar username dado un ID</span>

<span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">getUserNameById</span>(<span class="hljs-params">id: <span class="hljs-built_in">number</span> | <span class="hljs-built_in">string</span></span>) </span>{
  <span class="hljs-comment">//* Lógica de negocio, find the user</span>
  <span class="hljs-keyword">return</span> id
}

<span class="hljs-comment">//* No da errores 😄</span>
getUserNameById(<span class="hljs-number">10</span>)
getUserNameById(<span class="hljs-string">'10'</span>)

<span class="hljs-comment">//--------------------------------------------------------------------------------------------</span>

<span class="hljs-comment">//* Alias de tipos de TS</span>
<span class="hljs-keyword">type</span> IdUser = <span class="hljs-built_in">number</span> | <span class="hljs-built_in">string</span>
<span class="hljs-keyword">type</span> UserName = <span class="hljs-built_in">string</span>
<span class="hljs-keyword">let</span> userID: IdUser
idUser = <span class="hljs-number">10</span>
idUser = <span class="hljs-string">'10'</span>

<span class="hljs-comment">//* Buscar username dado un ID</span>

<span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">getUserName</span>(<span class="hljs-params">id: IdUser</span>): <span class="hljs-title">UserName</span> </span>{
  <span class="hljs-comment">//* Lógica de negocio, find the user</span>
  <span class="hljs-keyword">return</span> <span class="hljs-string">'Mike'</span>
}

<span class="hljs-comment">//----------------------------------------------------------------------------------------------------</span>

<span class="hljs-comment">//* Tipos literales</span>
<span class="hljs-comment">//* Por ejemplo, sólo aceptamos fotos de 100x100, 500x500 y 1000x1000</span>
<span class="hljs-keyword">type</span> SquareSize = <span class="hljs-string">'100x100'</span> | <span class="hljs-string">'500x500'</span> | <span class="hljs-string">'1000x1000'</span>
<span class="hljs-comment">//! let smallPicture: SquareSize = "200x200" //!Esto da un error, no está incluido ese valor en SquareSize</span>
<span class="hljs-keyword">let</span> smallPicture: SquareSize = <span class="hljs-string">"100x100"</span>
<span class="hljs-keyword">let</span> mediumPicture: SquareSize = <span class="hljs-string">"500x500"</span>
<span class="hljs-keyword">let</span> bigPicture: SquareSize = <span class="hljs-string">"1000x1000"</span>

<span class="hljs-built_in">console</span>.log(<span class="hljs-string">'small Picture: '</span>, smallPicture)
<span class="hljs-built_in">console</span>.log(<span class="hljs-string">'medium Picture: '</span>, mediumPicture)
<span class="hljs-built_in">console</span>.log(<span class="hljs-string">'big Picture: '</span>, bigPicture)
</code></pre>

# Aserciones de tipo

<p><a href="https://www.typescriptlang.org/docs/handbook/release-notes/typescript-1-6.html#new-tsx-file-extension-and-as-operator" rel="nofollow noopener" target="_blank">Documentación oficial sobre &lt;&gt; y ‘as’ en TS</a></p>
<p>Se recomienda más usar <code>as</code> ya que <code>&lt;&gt;</code> trae problemas con React</p>

---------------------------------------------------------

<p>Si tienes alguna función de alguna librería que no sabes que parámetros obtiene, y quieres guardarlo en un un objeto aparte, puedes usar</p>
<pre><code><span class="hljs-keyword">const</span> parameters: <span class="hljs-type">Parameters</span>&lt;typeof function&gt;[<span class="hljs-number">0</span>] = <span class="hljs-meta">{...}</span>
</code></pre>
<p>Y esto creara un objeto del tipo exacto que recibe ese objeto, teniendo las ventajas de autocompletado y evitar errores que se tendría creando un objeto dentro de la función.</p>

JSX support
JSX is an embeddable XML-like syntax. It is meant to be transformed into valid JavaScript, but the semantics of that transformation are implementation-specific. JSX came to popularity with the React library but has since seen other applications. TypeScript 1.6 supports embedding, type checking, and optionally compiling JSX directly into JavaScript.

New .tsx file extension and as operator
TypeScript 1.6 introduces a new .tsx file extension. This extension does two things: it enables JSX inside of TypeScript files, and it makes the new as operator the default way to cast (removing any ambiguity between JSX expressions and the TypeScript prefix cast operator). For example:

    var x = <any>foo;
    // is equivalent to:
    var x = foo as any;

Using React
To use JSX-support with React you should use the React typings. These typings define the JSX namespace so that TypeScript can correctly check JSX expressions for React. For example:

    /// <reference path="react.d.ts" />
    interface Props {
      name: string;
    }
    class MyComponent extends React.Component<Props, {}> {
      render() {
        return <span>{this.props.name}</span>;
      }
    }
    <MyComponent name="bar" />; // OK
    <MyComponent name={0} />; // error, `name` is not a number


# interfaces

<p>Propiedades opcionales : No todas las propiedades de una interfaz podrian ser requeridas. Usamos el simbolo ‘?’ luego del nombre de la propiedad.</p>
<pre><code>interface <span class="hljs-decorator">PictureConfig</span> {
	<span class="hljs-attribute">title</span>: <span class="hljs-built_in">string</span>;
	<span class="hljs-built_in">date</span>?: <span class="hljs-built_in">string</span>;
	<span class="hljs-attribute">orientation</span>: PhotoOrientation
}

</code></pre>
<p>Propiedades de solo lectura: Algunas propiedades de la interfaz podrian no ser modificables una vez creado el objeto. Esto es posible usando <strong>readonly</strong> antes del nombre de la propiedad.</p>
<pre><code>interface <span class="hljs-decorator">User</span> {
	readonly <span class="hljs-attribute">id:</span><span class="hljs-emphasis"> number</span>;
	<span class="hljs-attribute">username</span>: <span class="hljs-built_in">string</span>;
	readonly <span class="hljs-attribute">isPro</span>:<span class="hljs-attribute">boolean;
}
</span></code></pre>


# Extension de interfaces

<p>Al declarar una variable de esta forma:</p>
<pre><code><span class="hljs-keyword">let</span> <span class="hljs-keyword">variable</span>: number = <span class="hljs-number">2</span>;
</code></pre>
<p>Estás diciendo que ese espacio de memoria solo puede almacenar valores de tipo <code>number</code>.<br>
.<br>
Al hacerlo de esta otra forma (aserción):</p>
<pre><code><span class="hljs-keyword">let</span> <span class="hljs-keyword">variable</span> = <span class="hljs-number">2</span> <span class="hljs-keyword">as</span><span class="hljs-built_in"> number</span>;
</code></pre>
<p>Estás diciendo que el valor (2) que se le está asignando a la variable es de tipo <code>number</code>, pero la variable puede contener cualquier otro tipo de dato. Es solo para ser más explícito y tener autocompletado, realmente no estas tipando la variable, solo estás declarando de que tipo es el dato que le estás asignando en ese momento.</p>

# Clases publicas y privadas

<p><strong>private</strong> vs <strong># (hash)</strong><br>
&lrm;</p>
<p>&lrm;<br>
<strong>PRIVATE</strong><br>
Cuando usas la palabra reservada <strong>private</strong> en un atributo, e intentas acceder a ella fuera de la clase, el compilador de typescript te dará un error. Hay varias formas de evitar este error, por ejemplo, usando una aserción de tipo:</p>
<pre><code><span class="hljs-keyword">const</span> miObjeto = <span class="hljs-keyword">new</span> MiClase();
<span class="hljs-built_in">console</span>.log((miObjeto <span class="hljs-keyword">as</span> <span class="hljs-built_in">any</span>).propiedadPrivada);
</code></pre>
<p>Esto no da errores, y ebn cambio <strong>sí se puede acceder a la propiedad privada</strong> a la hora de ejecutar el archivo JavaScript.<br>
&lrm;<br>
&lrm;<br>
<strong># (HASH)</strong><br>
En cambio, con el # hash, que hace parte de la sintaxis de Ecmascript 2015, vuelve al atributo realmente privado, incluso en el archivo JavaScript que genera el compilador.<br>
De igual forma, si sigues intentando acceder al atributo fuera de la clase, typescript te arrojará un error.<br>
&lrm;<br>
<a href="https://stackoverflow.com/questions/59641564/what-are-the-differences-between-the-private-keyword-and-private-fields-in-types" rel="nofollow noopener" target="_blank">https://stackoverflow.com/questions/59641564/what-are-the-differences-between-the-private-keyword-and-private-fields-in-types</a></p>

# Métodos Get y Set

<p>Creo que una mejor manera de apreciar que sí estamos usando los métodos GET y SET es escribir un nombre diferente al del atributo, a pesar de haber usado el guión bajo.<br>
Ejemplo:</p>
<pre><code class="language-typescript"><span class="hljs-keyword">get</span> <span class="hljs-title">idPicture</span>(){
    <span class="hljs-keyword">return</span> <span class="hljs-built_in">this</span>._id
  }
  <span class="hljs-keyword">set</span> <span class="hljs-title">idPicture</span>(<span class="hljs-params">id: <span class="hljs-built_in">number</span></span>){
    <span class="hljs-built_in">this</span>._id = id
  }
  <span class="hljs-keyword">get</span> <span class="hljs-title">titlePicture</span>(){
    <span class="hljs-keyword">return</span> <span class="hljs-built_in">this</span>._title
  }
  <span class="hljs-keyword">set</span> <span class="hljs-title">titlePicture</span>(<span class="hljs-params">title: <span class="hljs-built_in">string</span></span>){
    <span class="hljs-built_in">this</span>._title = title
  }
  <span class="hljs-keyword">get</span> <span class="hljs-title">orientationPicture</span>(){
    <span class="hljs-keyword">return</span> <span class="hljs-built_in">this</span>._orientation
  }
  <span class="hljs-keyword">set</span> <span class="hljs-title">orientationPicture</span>(<span class="hljs-params">orientation: PhotoOrientation</span>){
    <span class="hljs-built_in">this</span>._orientation = orientation
  }

<span class="hljs-keyword">get</span> <span class="hljs-title">idAlbum</span>(){
    <span class="hljs-keyword">return</span> <span class="hljs-built_in">this</span>._id
  }
  <span class="hljs-keyword">set</span> <span class="hljs-title">idAlbum</span>(<span class="hljs-params">id: <span class="hljs-built_in">number</span></span>){
    <span class="hljs-built_in">this</span>._id = id
  }
  <span class="hljs-keyword">get</span> <span class="hljs-title">titleAlbum</span>(){
    <span class="hljs-keyword">return</span> <span class="hljs-built_in">this</span>._title
  }
  <span class="hljs-keyword">set</span> <span class="hljs-title">titleAlbum</span>(<span class="hljs-params">title: <span class="hljs-built_in">string</span></span>){
    <span class="hljs-built_in">this</span>._title = title
  }
</code></pre>
<p>Al momento de escribir el nombre del objeto, no vamos a acceder al atributo, sino al método:</p>
<pre><code class="language-typescript"><span class="hljs-comment">//* Estamos usando los métodos get y set y, no los atributos directamente</span>
picture.idPicture = <span class="hljs-number">100</span>
picture.titlePicture = <span class="hljs-string">'Another title'</span>
album.titleAlbum = <span class="hljs-string">'Title for an album'</span>
<span class="hljs-built_in">console</span>.table({album})
</code></pre>

# Clases Herencia, propiedades estaticas

<p>Veo que no han mostrado la diferencia entre public, protected y private, aquí les dejo las diferencias en Java (que me imagino se tomaron como base para otros lenguajes de POO como TS):</p>
<table>
<thead>
<tr>
<th></th>
<th>Class</th>
<th>Package</th>
<th>Subclass(same pkg)</th>
<th>Subclass(diff pkg)</th>
<th>World</th>
</tr>
</thead>
<tbody>
<tr>
<td>public</td>
<td>+</td>
<td>+</td>
<td>+</td>
<td>+</td>
<td>+</td>
</tr>
<tr>
<td>protected</td>
<td>+</td>
<td>+</td>
<td>+</td>
<td>+</td>
<td></td>
</tr>
<tr>
<td>no modifier</td>
<td>+</td>
<td>+</td>
<td>+</td>
<td></td>
<td></td>
</tr>
<tr>
<td>private</td>
<td>+</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

# Principios de responsabilidad única

<p>Principio de responsabilidad unica. Idealmente un archivo deberia tener un proposito o responsabilidad unica: definir una clase, una interfaz, un enumerado, etc.<br>
Esto mejora la legibilidad de codigo, facilita la lectura, testing y favorece su mantenimiento.</p>
<p>Utilizamos archivos separados y la utilizacion de import, export para lograr un poco mas de mantenibiilidad. Podemos usar tambien carpetas para separar nuestros archivos.</p>
<p>Para observar una carpeta entera usamos:</p>
<pre><code><span class="hljs-comment">tsc</span> <span class="hljs-literal">-</span><span class="hljs-literal">-</span><span class="hljs-comment">project</span> <span class="hljs-comment">myFolder</span> <span class="hljs-literal">-</span><span class="hljs-literal">-</span><span class="hljs-comment">watch</span>
</code></pre>

--------------------------------

es uso de la función findIndex() es útil para encontrar la posición de un ítem dentro de un arreglo, pero para el caso de eliminar dicho ítem del arreglo prefiero utilizar filter() que te retorna un nuevo arreglo sin el ítem a eliminar y evita así hacer validaciones y utilizar más funciones.