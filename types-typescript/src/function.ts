export {};
// Crear una fotografia
/*function createPicture(title, date, size) {
  title
}*/

type SquareSize = '100x100' | '500x500' | '1000x1000';

// Usamos TS, definimos tipos para parametros
function createPicture(title:string, date:string, size: SquareSize) {
  // Se crea la fotografia
  console.log('Create picture usando', title, date, size);
}

createPicture('My Birthay', '2021-03-10', '500x500');

// Parametros opcionales en funciones

function createPictureB(title?:string, date?:string, size?: SquareSize) {
  // Se crea la fotografia
  console.log('Create picture usando', title, date, size);
}

createPictureB('My Birthay', '2021-03-10');

// Flat Array Function
let createPic = (title: string, date:string, size: SquareSize = '500x500'): object => {
  return {
    title,
    date,
    size,
  }
};

const picture = createPic('PLatzi session', '2020-03-10');
console.log('picture', picture);

function apply(items: number[], fn:(item: number) => number): number{
  ///....
  console.log(items, fn)
  return 4545;
}

// Tipo de retorno con TS
function handleError(code:number, message: string):never | string {
  // Procesamiento del codigo, mensaje
  if (message === 'error') {
    throw new Error(`${message}. Code error: ${code}`);    
  }
  else {
    return 'An error has occurred';
  }
}

try {
  let result = handleError(200, 'OK'); // string
  console.log(result);
  result = handleError(400, 'error'); // never
  console.log(result);
} catch (error) {
  console.log(error, '----------')
}
