export {};
// Funcion para mostrar una fotografia

enum PhotoOrientation {
  Landscape, // 0
  Portrait, // 1
  Square, // 2
  Panorama // 3
}

interface Entity {
  id: number,
  title: string,
}

interface Album extends Entity {
  // copia atributos Entity
  description: string;
}

interface Picture extends Entity {
  // copia atributos Entity
  orientation: PhotoOrientation;
}

const album: Album = {
  id: 1,
  title: 'Meetups',
  description: 'Community events around the world'
};

const picture: Picture = {
  id: 1,
  title: 'foto',
  orientation: PhotoOrientation.Panorama,
};

let newPicture = {} as Picture;
newPicture.id = 2;
newPicture.title = 'Moon';

console.log(album)
console.log(picture)
console.log(newPicture)