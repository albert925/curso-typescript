export {};

enum PhotoOrientation {
  Landscape, // 0
  Portrait, // 1
  Square, // 2
  Panorama // 3
}
// Super clase
abstract class Item {
  protected readonly _id:number;
  protected _title: string;

  constructor(id: number, title:string){
    this._id = id;
    this._title = title;
  }

  get id(){
    return this._id;
  }

  get title(){
    return this._title;
  }

  /*set id(id:number){
    this._id = id;
  }*/ // Error el id es de solo lectura

  set title(title:string){
    this._title = title;
  }
}

// get y set

class Picture extends Item {
  // Propiedades
  public static photoOrientation = PhotoOrientation;
  private _orientation: PhotoOrientation;

  constructor(
    id: number,
    title: string,
    orientation: PhotoOrientation = PhotoOrientation.Panorama
  ) {
    super(id, title);
    this._orientation = orientation;
  }

  set ortientation(ortientation:PhotoOrientation){
    this._orientation = ortientation;
  }

  // Comportamiento
  toString() {
    return `[id: ${this.id},title:${this._title},orientation: ${this._orientation}]`;
  }
}

class Album extends Item {
  private pictures: Picture[];

  constructor(
    id: number,
    title: string,
  ) {
    super(id, title);
    this.pictures = [];
  }

  addPicture(picture: Picture){
    this.pictures.push(picture);
  }
}

const album: Album = new Album(1, 'Personale Pictures');
const picture:Picture = new Picture(1, 'PLatzi session', PhotoOrientation.Square);
album.addPicture(picture);

console.log('album', album);

// Accediendo a lo miembros publicos
console.log(picture.id); // get id()

//picture.id = 100; // private, set id(100) // Error es solo lectura;
picture.title = 'VVVV'; // private set title('VVV')
album.title = 'Album name'
console.log('album', album);

// const item = new Item(1, 'huhuh'); Error
// console.log(item, 'item') Error

// Probar el meimbro estatico 
console.log('photoOrientation', Picture.photoOrientation.Landscape);
