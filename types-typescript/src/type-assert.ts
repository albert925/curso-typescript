// <tipo> // Angle Bracket syntax
let username: any;
username = 'nombreuser';

// tenemos una cadena, TS confia en mi!
let message: string = (<string>username).length > 5 ? 
                      `Welcome ${username}` : 
                      'Username is too short';
console.log('message', message);

// En React  sintasis "as"---------------------------------------
let messageB: string = (username as string).length > 5 ? 
                      `Welcome ${username}` : 
                      'Username is too short';
console.log('messageB', messageB);

let usernameWithID: any = 'name user 1';
// Como obtener el username?
username = (<string>usernameWithID).substring(0, 10);
console.log(username);
// En react sintasis "as"---
username = (usernameWithID as string).substring(0, 10);
console.log(username);

let helloUser: any;
helloUser  = 'hello paparazzi';
username = (helloUser as string).substring(6);
console.log(username);
