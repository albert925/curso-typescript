// Void

// tipo explicito
function showInfo(user:any): any {
  console.log('User info', user.id, user.username, user.firstName);
}

showInfo({id: 1, userName: 'nombre', firstName: 'fir'});

// Tipo Inferido
function showFormattedInfo(user: any) {
  console.log('User info', `
    id: ${user.id},
    username: ${user.username},
    firstname: ${user.firstName}
  `);
}

showFormattedInfo({id: 1, username: 'nombre', firstName: 'fir'});

// tipo void, como tipo de dato en variable
let unusable: void;
//unusable = null;
unusable = undefined;

// Tipo: Never
function handleError(code:number, message: string): never {
  //Procesamiento de tu codigo
  //Generamos un mensaje
  throw new Error(`${message}. Code: ${code}`);
}

/*try {
  handleError(404, 'Not found');
} catch (error) {
  console.log(error);
}*/

function sunNumbers(limit:number): never {
  let suma = 0;
  while (true) {
    suma++;
  }
}

sunNumbers(10);
//ciclo infinito
