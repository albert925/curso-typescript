// Corchetes []

// Tipo explicito
let users: string[];
users = ['aa', 'bb', 'cc'];
//users = [1, true, 'cc']; // Error

// Tipo inferido
let otherUsers = ['aa', 'bb', 'cc'];
//users = [1, true, 'cc']; // Error

// Arrat<Tipo>
let picturesTitles: Array<string>;
picturesTitles = ['f1','f2','f3','f4'];

//Accediendo a los valores de un Array
console.log('first user', users[0]);
console.log('first user', picturesTitles[0]);

// Propiedades en Array
console.log(users.length);

// Uso de funciones en Arrays
users.push('casjsjs');
users.sort();
console.log('users', users);