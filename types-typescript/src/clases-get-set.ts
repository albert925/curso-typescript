export {};

enum PhotoOrientation {
  Landscape, // 0
  Portrait, // 1
  Square, // 2
  Panorama // 3
}

// get y set

class Picture {
  // Propiedades
  private _id: number;
  private _title: string;
  private _orientation: PhotoOrientation;

  constructor(
    id: number,
    title: string,
    orientation: PhotoOrientation = PhotoOrientation.Panorama
  ) {
    this._id = id;
    this._title = title;
    this._orientation = orientation;
  }

  get id(){
    return this._id;
  }

  get title(){
    return this._title;
  }

  set id(id:number){
    this._id = id;
  }

  set title(title:string){
    this._title = title;
  }

  set ortientation(ortientation:PhotoOrientation){
    this._orientation = ortientation;
  }

  // Comportamiento
  toString() {
    return `[id: ${this.id},title:${this._title},orientation: ${this._orientation}]`;
  }
}

class Album {
  private _id: number;
  private _title: string;
  private pictures: Picture[];

  constructor(
    id: number,
    title: string,
  ) {
    this._id = id;
    this._title = title;
    this.pictures = [];
  }

  addPicture(picture: Picture){
    this.pictures.push(picture);
  }
}

const album: Album = new Album(1, 'Personale Pictures');
const picture:Picture = new Picture(1, 'PLatzi session', PhotoOrientation.Square);
album.addPicture(picture);

console.log('album', album);

// Accediendo a lo miembros publicos
console.log(picture.id); // get id()

picture.id = 100; // private, set id(100);
picture.title = 'VVVV'; // private set title('VVV')
