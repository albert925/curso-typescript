// number
// explicito
let phone: number;
phone = 1;
phone = 232332;


// Inferido
let phoneNumber = 3434344;
phoneNumber = 666;

let hex: number = 0xf00d;
let binary: number = 0b01010001;
let octal: number = 0o12312;
let octalb: number = 0o7444;

// tipo boolean
let isPro: boolean;
isPro = true;

// Inferido
let isUserPro = false;


//strings
let userName: string = 'Name';
userName = 'name 2';

//template string
// uso de backt-tick `
let salute: string = `Hola, soy ${userName} 
  y soy ${isUserPro}`;

console.log(salute);