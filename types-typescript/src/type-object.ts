// Type: object
let user: object;
user = {}; // Object

user = {
  id: 1,
  userName: 'usern',
  firstName: 'Pablo',
  isPro: true,
};

console.log(user);
// Object vs objecdt(Clase JS vs tipo TS)
const myObj = {
  id: 1,
  userName: 'usern',
  firstName: 'Pablo',
  isPro: true,
};

const isInstance = myObj instanceof Object; // Casle Object JS
console.log('isInstance', isInstance);
//console.log('user.username', user.userName); /7 Error
console.log('user.username', myObj.userName);