export {};

enum PhotoOrientation {
  Landscape, // 0
  Portrait, // 1
  Square, // 2
  Panorama // 3
}

class Picture {
  // Propiedades
  private id: number;
  private title: string;
  private orientation: PhotoOrientation;

  constructor(
    id: number,
    title: string,
    orientation: PhotoOrientation = PhotoOrientation.Panorama
  ) {
    this.id = id;
    this.title = title;
    this.orientation = orientation;
  }

  // Comportamiento
  toString() {
    return `[id: ${this.id},title:${this.title},orientation: ${this.orientation}]`;
  }
}

class Album {
  private id: number;
  private title: string;
  private pictures: Picture[];

  constructor(
    id: number,
    title: string,
  ) {
    this.id = id;
    this.title = title;
    this.pictures = [];
  }

  addPicture(picture: Picture){
    this.pictures.push(picture);
  }
}

const album: Album = new Album(1, 'Personale Pictures');
const picture:Picture = new Picture(1, 'PLatzi session', PhotoOrientation.Square);
album.addPicture(picture);

console.log('album', album);

// Accediendo a lo miembros publicos
// picture.id = 100; // Error
// picture.title = 'VVVV'; // Error
