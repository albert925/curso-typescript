export {};
// [1, 'user']
let user: [number, string];
user = [1, 'name'];

console.log('user', user);
console.log('username', user[1]);
console.log('id', user[0]);

// Tuplas con varios valores
// id, username, isPro
let userInfo: [number, string, boolean];
userInfo = [2, 'nameB', true];

console.log('userinfo', userInfo);

// Arreglo de tuplas
let array: [number, string][] = [];
array.push([1, 'bb']);
array.push([1, 'cc']);
array.push([1, 'dd']);
console.log(array);

// uso de funciones Array
// dd -> dd001

array[2][1] = array[2][1].concat('001');
console.log('---2',array);