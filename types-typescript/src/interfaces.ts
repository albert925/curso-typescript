export {};
// Funcion para mostrar una fotografia

enum PhotoOrientation {
  Landscape, // 0
  Portrait, // 1
  Square, // 2
  Panorama // 3
}

interface Picture {
  title: string,
  date: string,
  orientation: PhotoOrientation
}

function showPicture(picture: Picture) {
  console.log(`[
    title:${picture.title},
    date: ${picture.date},
    orientation: ${picture.orientation}
  ]`)
}

let myPic = {
  title: 'test title',
  date: '2020-03-03',
  orientation: PhotoOrientation.Landscape,
};

showPicture(myPic);

interface PictureConfig {
  title?: string,
  date?: string,
  orientation?: PhotoOrientation
}

function generatePicture(config: PictureConfig) {
  const pic = {title: 'Default', date: '2020-03-03'}
  if (config.title) {
    pic.title = config.title;
  }
  if (config.date) {
    pic.date = config.date;
  }

  return pic;
}

let pictureB = generatePicture({});
console.log('pictureB', pictureB);
pictureB = generatePicture({title: 'name 22222'});
console.log('pictureB', pictureB);

// Intefaz: Usuario

interface User {
  readonly id: number; // solo lectura
  username: string;
  isPro: boolean;
}

let user: User;
user = {id: 10, username: 'nombre', isPro: false};
console.log(user);
user.isPro = true;
user.username = 'nmame 4444';
//user.id = 54; // Error
console.log(user);