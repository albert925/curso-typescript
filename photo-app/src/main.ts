import { User } from "./user";
import { Album } from "./album";
import { Picture } from "./picture";

const user = new User(1, 'user', 'name');
const album = new Album(1, 'album mu');
const picture = new Picture(1, 'foto', '2021-03-03');

// Creamos relaciones
user.addAlbum(album);
album.addPicture(picture);

console.log('user', user);

user.removeAlbum(album);

console.log('user', user);